const schema = `

type ListingType{
     name: String
}

type ListingActivities{
     name: String
}

type Guide {
     Name: String
     Bio: String
     Avatar: String
}

type Listing{
     listingId: String
     coverPhoto: String
     listingName: String
     listingDescription: String
     listingType: [ListingType]
     listingLocation: String
     listingActivities: [ListingActivities]
     specialType: String
     specialAmount: Int
     rating: Int
     guide: Guide
     price: String
     numberOfDays: Int
}

type Booking {
     bookingId: String
     listingId: String
     bookingDate: String
     bookingTotal: String
     customerID: String
     customerEmail: String
     customers: [Customer]
     chargeReciept: String
}



type Customer {
     name: String
     Surname: String
     country: String
     passportNumber: String
     physioScore: String
}


input InputCustomer {
     name: String
     Surname: String
     country: String
     passportNumber: String
     physioScore: String
}

type User{
     id: ID!
     email: String!
}

type LoginResponse{
     accessToken: String,
     refreshToken: String,
}


"""
A hello world Query
"""

type Query {
     getAllListings: [Listing]
     getAListing(listingId: String!) : Listing!
     getCustomerBookings(customerID:String!): [Booking!]
}

type Mutation {
     makeABooking(
     listingId: String
     bookingDate: String,
     customerID: String,
     customerEmail: String,
     customers: [InputCustomer]
     ): Booking

     register(
          email: String!,
          password: String!
          ): User

     login(
          email: String!,
          password: String!
     ): LoginResponse




}


`


export {schema}

import { v1 as uuidv1} from "uuid"
import stripePackage from "stripe"
import AWS from "aws-sdk"
import  dynamoDBLib from "../../libs/dynamodb-lib"

const Cognito = new AWS.CognitoIdentityServiceProvider({
    region: "us-east-1",
})
// console.log(`START.. ${Cognito}`)
// console.log(Object.keys(Cognito))
export const makeABooking = async (args, context) => {
    //Get the listing that the user selected
    //from the client
    const getPrices = async () => {
    const params = {
        TableName: process.env.ListingsDB || "peggys-lunar-tour-api-dev-BookingsDB-1SVT41MBVBRSJ",
        KeyConditionExpression: "listingId = :listingId",
        ExpressionAttributeValues: {
        ":listingId": args.listingId,
        },
    };
    
    try {
        const listings = await dynamoDBLib.query(params);
        return listings;
    } catch (e) {
        return e;
    }
    };

    const listingObject = await getPrices();

    

    //caLCULATE THE amount to be charged to the
    //customers card

    const bookingCharge = parseInt(listingObject.Items[0].price) * args.customers.length
    //get the name of the listing

    const listingName = listingObject.listingName;
    //create an instance of the stripe lib

    const stripe = stripePackage(process.env.stripeSecretKey);

    //charge the users card

    const stripeResult = await stripe.charges.create({
    source: "tok_visa",
    amount: bookingCharge,
    description: `Charge for booking of listing ${args.listingId}`,
    currency: "usd",
    });


    const params = {
    TableName: process.env.BookingsDB || "peggys-lunar-tour-api-dev-BookingsDB-1SVT41MBVBRSJ",

    Item: {
        bookingId: uuidv1(),
        listingId: args.listingId,
        bookingDate: args.bookingDate,
        size: args.customers.length > 0 ? args.customers.length : 0,
        bookingTotal: bookingCharge,
        customerEmail: args.customerEmail,
        customerID: args.customerID,
        customers: args.customers,
        createdTimestamp: Date.now(),
        chargeReciept: stripeResult.receipt_url,
        paymentDetails: stripeResult.payment_method_details,
    },
    };
    
    
    try {
      //insert the booking into the table
    await dynamoDBLib.put(params);

    
    return {
        bookingId: params.Item.bookingId,
        listingId: params.Item.listingId,
        bookingDate: params.Item.bookingDate,
        size: args.customers.length> 0 ? args.customers.length : 0,
        bookingTotal: params.Item.bookingTotal,
        customerEmail: params.Item.customerEmail,
        customerID: params.Item.customerID,
        customers: params.Item.customers.map((c) => ({
        name: c.name,
        Surname: c.Surname,
        country: c.country,
        passportNumber: c.passportNumber,
        physioScore: c.physioScore,
        })),
        chargeReciept: params.Item.chargeReciept,
    };
    } catch (e) {
    return e;
    }
};




export const login = async (args, context) => {
    const params = {
      AuthFlow: "ADMIN_USER_PASSWORD_AUTH" /* required */,
      ClientId: 'qi0u8ofq8l9pm4n75gvijvvll' /* required */,
    UserPoolId: process.env.userpool || "us-east-1_8UadveXt4",
    AuthParameters: {
        USERNAME: args.email,
        PASSWORD: args.password
    },
    };
    
    try {
    
    const result = await Cognito.adminInitiateAuth(params).promise();
    
    
    const AccessToken = {
        jwt: result.AuthenticationResult.AccessToken,
        expiry: result.AuthenticationResult.ExpiresIn,
    };
    
    const Session = {
        accessToken: JSON.stringify(AccessToken),
        refreshToken: result.AuthenticationResult.RefreshToken,
    };
    
    return Session;
    } catch (e) {
        //return e.message;
    console.log('error',e);
    //throw new ApolloError(e.message);
    }
};


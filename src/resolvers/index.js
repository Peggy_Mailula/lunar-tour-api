import {getAllListings,getAListing,getCustomerBookings} from "./query";
import {login, makeABooking} from "./mutation";

export const resolvers = {
    Query: {
        getAllListings: (root, args, context) => getAllListings(args, context),
        getAListing: (root, args, context) => getAListing(args, context),
        getCustomerBookings: (root, args, context) => getCustomerBookings(args, context)
    
        
    },
    Mutation: {
        makeABooking: (root,args,context) => makeABooking(args, context),
        login: (root,args,context) => login(args,context)

    },
}

import  dynamoDBLib from "../../libs/dynamodb-lib"


export const getAllListings = async (args, context) => {
    const params = {
        TableName: process.env.ListingsDB || "peggys-lunar-tour-api-dev-ListingsDB-W91XB6POP1F6",
    }
    console.log(params)
    try {

        const result = await dynamoDBLib.scan(params)

        if (result.Items.length === 0) {
        return []
        } else {
        return result.Items.map(i => ({
            listingId: i.listingId,
            coverPhoto: i.coverPhoto,
            listingName: i.listingName,
            listingDescription: i.listingDescription,
            listingType: i.listingType.map(m => ({
            name: m,
            })),
            listingLocation: i.listingLocation,
            listingActivities: i.listingActivities.map(k => ({
            name: k,
            })),
            specialType: i.specialType,
            specialAmount: i.specialAmount,
            rating: i.rating,
            guide: {
            Name: i.guide.name,
            Bio: i.guide.bio,
            Avatar: i.guide.avatar,
            },
            price: i.price,
            numberOfDays: i.numberOfDays,
        }))
        }
    
        // return result;
    } catch (e) {
        return {
        message: e.message,
        code: "500x",
        }
    }

}

export const getAListing = async (args, context) => {
    const params = {
    TableName: process.env.ListingsDB || "peggys-lunar-tour-api-dev-ListingsDB-W91XB6POP1F6",
    FilterExpression: "listingId = :listingId",
    ExpressionAttributeValues: {
        ":listingId": args.listingId,
    },
    };
    

    try {
    const listing = await dynamoDBLib.scan(params);

    console.log(listing);

    if (listing.Items.length === 0) {
        return [];
    } else {
        return {
        listingName: listing.Items[0].listingName,

        listingId: listing.Items[0].listingId,
        coverPhoto: listing.Items[0].coverPhoto,
        listingDescription: listing.Items[0].listingDescription,
        listingType: listing.Items[0].listingType.map((m) => ({
            name: m,
        })),
        listingLocation: listing.Items[0].listingLocation,
        listingActivities: listing.Items[0].listingActivities.map((k) => ({
            name: k,
        })),
        specialType: listing.Items[0].specialType,
        specialAmount: listing.Items[0].specialAmount,
        rating: listing.Items[0].rating,
        guide: {
            Name: listing.Items[0].guide.name,
            Bio: listing.Items[0].guide.bio,
            Avatar: listing.Items[0].guide.avatar,
        },
        price: listing.Items[0].price,
        numberOfDays: listing.Items[0].numberOfDays,
        };
    }
    } catch (e) {
    return {
        message: e.message,
        code: "500x",
    };
    }
};


export const getCustomerBookings = async (args, context) => {
    const params = {
        TableName: process.env.BookingsDB || "peggys-lunar-tour-api-dev-BookingsDB-1SVT41MBVBRSJ",
    
    };
    
    console.log(params)
    try {

        const  userBookings= await dynamoDBLib.scan(params);
        const processedArray = userBookings.Items.filter(booking => booking.customerID === args.customerID)
        return processedArray

    }
    
    
    catch (e) {
    return {
        message: e.message,
        code: "500x",
    };
    }
};
